#!/bin/bash

source "$(dirname "$0")/common.sh"

info "Starting pipe..."

DEBUG=${DEBUG:="false"}
enable_debug
if [[ "${DEBUG}" == "true" ]]; then
  extra_args="--verbose"
fi

# mandatory variables
IBM_USER=${IBM_USER:?'IBM_USER environment variable missing.'}
IBM_PASSWORD=${IBM_PASSWORD:?'IBM_PASSWORD environment variable missing.'}
IBM_REGION=${IBM_REGION:?'IBM_REGION environment variable missing.'}
IBM_CLUSTER=${IBM_CLUSTER:?'IBM_CLUSTER environment variable missing.'}
IBM_K8S_DEPLOYMENT=${IBM_K8S_DEPLOYMENT:?'IBM_K8S_DEPLOYMENT environment variable missing.'}

info "Logging in to IBM cloud"
run ibmcloud config --check-version=false
run ibmcloud login -u ${IBM_USER} -p ${IBM_PASSWORD} -r ${IBM_REGION}

info "Retrieving Kubernetes config file"
run ibmcloud ks cluster config --cluster ${IBM_CLUSTER} --export
source $output_file

info "Kubernetes config file stored at"
run env | grep KUBECONFIG

info "Trying to get pods, just to see if kubectl works correctly"
run kubectl get pods

info "Performing rollout"
run kubectl rollout restart deployment/${IBM_K8S_DEPLOYMENT}

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi

