# Bitbucket Pipelines Pipe: IBM Kubernetes rollout

A pipe that uses IBM cloud tools and kubectl to interact with a Kubernetes cluster running on [IBM cloud](https://www.ibm.com/cloud/kubernetes-service/)

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: rgruber/ibm-k8s-pipe:2.0.0
    variables:
      IBM_USER: $IBM_USER
      IBM_PASSWORD: $IBM_PASSWORD
      IBM_REGION: $IBM_REGION
      IBM_CLUSTER: $IBM_CLUSTER
      IBM_K8S_DEPLOYMENT: $IBM_K8S_DEPLOYMENT
      # DEBUG: '<boolean>' # Optional
```

## Variables

| Variable                  | Usage                                                       |
| ------------------------- | ----------------------------------------------------------- |
| IBM_USER (*)              | The username or ID required for login to IBM cloud. |
| IBM_PASSWORD (*)          | The password required for login to IBM cloud. |
| IBM_REGION  (*)           | The region where your cluster is running. Required for login to IBM cloud. |
| IBM_CLUSTER (*)           | The name of your cluster. Needed to retrieve the Kubernetes config file.
| IBM_K8S_DEPLOYMENT (*)    | The name of the deployment for which a rollout should be triggered.  |
| DEBUG                     | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Details

This pipe will login to IBM cloud with the provided login credentials, retrieve the KUBECONFIG from the provided cluster and then trigger a rollout for the provided deployment.

```sh
ibmcloud login -u ${IBM_USER} -p ${IBM_PASSWORD} -r ${IBM_REGION}
ibmcloud ks cluster config --cluster ${IBM_CLUSTER} --export
kubectl rollout restart deployment/${IBM_K8S_DEPLOYMENT}
```

## Prerequisites

Your IBM Kubernetes Service must be correctly set up and a deployment must have been created previously and must be running as intended. This pipe will not create any new resources nor will it modify them. The pipe will only trigger a rollout restart. 

The intended use case is to have a deployment running which’ containers are configured to use a certain fixed image tag and the imagePullPolicy Always. This will ensure that during a restart the container will always fetch the latest image version. 

```yaml
spec:
  containers:
    - name: my-image
      image: docker.io/my-username/my-image:production
      imagePullPolicy: Always
```

The above example deployment spec shows the use of an image with the tag ‘production’. Every time a new version should be released the image will be tagged with the ‘production’ tag, essentially updating the ‘production’ tag to the latest version. When a rollout restart is triggered, kubernetes will fetch the latest ‘production’ image and use it to start new pods. Those new pods will then replace the existing pods (by default in a rolling-update).

## Examples

```yaml
script:
  - pipe: rgruber/ibm-k8s-pipe:2.0.0
    variables:
      IBM_USER: $IBM_USER
      IBM_PASSWORD: $IBM_PASSWORD
      IBM_REGION: $IBM_REGION
      IBM_CLUSTER: $IBM_CLUSTER
      IBM_K8S_DEPLOYMENT: $IBM_K8S_DEPLOYMENT
```

## Support
rgsoft{at}gmx.at

