FROM ibmcom/ibm-cloud-developer-tools-amd64

RUN apk add --update --no-cache bash

COPY pipe /
RUN chmod +x /pipe.sh

ENTRYPOINT ["/pipe.sh"]
