# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.0.0

- major: Fixed error in pipe definition

## 1.0.0

- major: Initial release

